#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

from Tkinter import *
import config as cfg
import tkFileDialog as filedialog
import tkMessageBox
#from accelerate6 import PaintCompGraph
import accelerate6 as ac6

def ButtonSaveClick():
	f=open("settings.ini","w")
	f.write(str(cfg.MIN_THROTTLE.get())+"\n")
	f.write(str(cfg.MAN_THROTTLE.get())+"\n")
	f.write(str(cfg.START_SPEED.get())+"\n")
	f.write(str(cfg.MIN_RPM.get())+"\n")
	f.close()
	cfg.MIN_THROTTLE_VALUE=cfg.MIN_THROTTLE.get()
	cfg.MAX_THROTTLE_VALUE=cfg.MAN_THROTTLE.get()
	cfg.START_SPEED_VALUE=cfg.START_SPEED.get()
	cfg.MIN_RPM_VALUE=cfg.MIN_RPM.get()
	

def ButtonExitClick():
	cfg.RUNING=0
	sys.exit()
	#print "Start"
def ButtonCompClick(form):
	cfg.GraphX=[]
	cfg.GraphY=[]

	fileSrc = filedialog.askopenfilename(parent=form,filetypes=[('RPM files','.csv'),('all files','.*')])

	if fileSrc:
		#try:
			cfg.RUNING=0
			ac6.GraphInit()
			fSrc=open(fileSrc,"r")
			num=fSrc.read()
			list=[]
			list=num.split()
			fSrc.close()
			listItems=len(list)
			print listItems
			for i in range(1,listItems):
				#cfg.GraphY.append(list[i])
				#print cfg.RPM
				list[i]=list[i][0:(list[i].find(","))]
				cfg.RPM=int(list[i])
				#print cfg.RPM
				cfg.GraphY.append(cfg.RPM)
				cfg.GraphX.append(i-1)
			cfg.GraphX[0]=10
			cfg.COUNT_ELEM=len(cfg.GraphY)
			ac6.PaintGraph()
			print len(cfg.GraphY)
			cfg.RUNING=1
		#except:
	#		tkMessageBox.showerror("Open Source File", "Failed to read file \n'%s'"%fileSrc)
	return 

def ButtonReadClick(form):
	cfg.COUNT_ELEM2=0
	cfg.fGraphY=[]
	#cfg.dGraphY=0

	fileDst = filedialog.askopenfilename(parent=form,filetypes=[('RPM files','.csv'),('all files','.*')])
	#print fileDst+"*"
	if fileDst:
		try: 
			fDst=open(fileDst,"r")
			num=fDst.read()
			list=[]
			list=num.split()
			fDst.close()
			cfg.COUNT_ELEM2=len(list)
			for i in range(1,cfg.COUNT_ELEM2):
				list[i]=list[i][0:(list[i].find(","))]
				cfg.fGraphY.append(int(list[i]))
		except:
			tkMessageBox.showerror("Open Source File", "Failed to read file \n'%s'"%fileDst)
	cfg.COUNT_ELEM2=len(cfg.fGraphY)
	ac6.PaintCompGraph()
	return                                                     
	
def ButtonCloseClick(form):
	form.destroy()


def CreateSetForm(MainForm):
	SetForm = Toplevel(MainForm)
	SetForm.title('child')
#	SetForm.geometry('480x300+0+0')
	SetForm.geometry('%dx%d+%d+%d' % (480, 320, 0, -8))
	#attributes('-fullscreen', True)
	SetForm.configure(cursor='none')
	SaveButton=Button(SetForm,text="Apply",command=ButtonSaveClick)
	ExitButton=Button(SetForm,text="Exit",command=ButtonExitClick)
	ReadButton=Button(SetForm,text="Destination",command=lambda:ButtonReadClick(SetForm))
	CompButton=Button(SetForm,text="Source",command=lambda:ButtonCompClick(SetForm))
	CloseButton=Button(SetForm,text="Close",command=lambda:ButtonCloseClick(SetForm))

	SaveButton.place(x=21,y=250,width=75,height=40)
	ExitButton.place(x=112,y=250,width=75,height=40)
	ReadButton.place(x=203,y=250,width=75,height=40)
	CompButton.place(x=294,y=250,width=75,height=40)
	CloseButton.place(x=385,y=250,width=75,height=40)

	MinThrottle = Scale(SetForm, from_=0, to=100, orient=HORIZONTAL, label="min throttle%",width=20,length=460)
	MinThrottle.place(x=10,y=0)
	cfg.MIN_THROTTLE=MinThrottle
	
	MaxThrottle = Scale(SetForm, from_=0, to=100, orient=HORIZONTAL, label="max throttle%",width=20,length=460)
	MaxThrottle.place(x=10,y=60)
	cfg.MAN_THROTTLE=MaxThrottle
	
	StartSpeed = Scale(SetForm, from_=0, to=200, orient=HORIZONTAL, label="start speed",width=20,length=460)
	StartSpeed.place(x=10,y=120)
	cfg.START_SPEED=StartSpeed

	StartRPM = Scale(SetForm, from_=1000, to=6000, orient=HORIZONTAL, label="min rpm",width=20,length=460)
	StartRPM.place(x=10,y=180)
	cfg.MIN_RPM=StartRPM

	MinThrottle.set(cfg.MIN_THROTTLE_VALUE)
	MaxThrottle.set(cfg.MAX_THROTTLE_VALUE)
	StartSpeed.set(cfg.START_SPEED_VALUE)
	StartRPM.set(cfg.MIN_RPM_VALUE)
