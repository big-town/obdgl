#!/usr/bin/python
# -*- coding: utf-8 -*- 

import sys
import  time

print "File is ",sys.argv
fn=sys.argv[1]
f = open(fn, "r")
lines=f.readlines()
params_old=lines[2].split(',')
startFlag=False
writeFlag=False
convert=[]
convert_full=[]
count=0
row=0
timeBegin=0.0
timeEnd=0.0
f.seek(0)
for line in f:
    row=row+1
    if line.find("GET")>=0:
        continue 
    params=line.split(',')
    if (not startFlag) and float(params[2]) < 3300 and params[5]=="Open load" and float(params[7]) >= 100 and float(params[10]) >= 82:
        startFlag=True
        writeFlag=False
        timeBegin=float(params[0])
    if startFlag:
        convert.append(params[2]+','+params[0]+',82.0'+"\n")
        convert_full.append(', '.join(params))
        #convert.append(params[2]+','+params[0]+','+params[10]+"\n")
        #convert.append(str(row)+','+params[2]+','+params[0]+','+params[10]+"\n")
    if startFlag and float(params[10]) < 50:
    #if startFlag and float(params_old[1])>float(params[1]):
        startFlag=False
        if float(params[2]) > 5000:
            writeFlag=True
    if (not startFlag) and writeFlag:
        convert.pop()
        count=count+1
        timeEnd=float(params[0])
        fw=open(fn[0:fn.index('.')]+'_'+str(round(timeEnd-timeBegin,2))+'-'+str(count)+'.csv','w')
        fw1=open(fn[0:fn.index('.')]+'_'+str(round(timeEnd-timeBegin,2))+'-'+str(count)+'-full.csv','w')
        fw.write("RPM,Time,TPS\n")
        fw.writelines(convert)
        fw1.writelines(convert_full)
        fw1.close()
        fw.close()
        writeFlag=False
        convert=[]
        convert_full=[]
        print  timeEnd-timeBegin
    params_old=params
f.close()