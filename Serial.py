#!/usr/bin/python
import time
import serial
import struct

ser = serial.Serial(
    port='/dev/ttyUSB1',
    baudrate=38400,
    timeout=0
)
countByte=0
#countW=0
#countR=0
FlagWrite=False
tb=0
while 1:
    #inW=ser.inWaiting()
    #outW=ser.out_waiting
    if (time.time()-tb)>1:
        FlagWrite=False
    if ser.inWaiting()==0 and ser.out_waiting==0 and not FlagWrite: 
        tb=time.time()
        ser.write('7')
        ser.flush()
        FlagWrite=True
        #countW=countW+1
    countByte=ser.inWaiting()
    if countByte>0:
        #countR=countR+1
        data = ser.read(1) 
        b=struct.unpack('B',data[0])
        print b[0], "%10.6f" % time.time()
        FlagWrite=False
        
