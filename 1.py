#!/usr/bin/python
from __future__ import division
# -*- coding: utf-8 -*-
import pygame

pygame.init();
clock = pygame.time.Clock()
screen = pygame.display.set_mode((480, 320))
pygame.display.toggle_fullscreen()
done = False
font = pygame.font.SysFont("comicsansms", 72)
text = font.render("Hello, World", True, (0, 128, 0))

while not done:
    if pygame.mouse.get_pressed()[1] or pygame.mouse.get_pressed()[2]:
	done = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    screen.fill((255, 255, 255))
    screen.blit(text,(100,100))
    pygame.display.flip()
    clock.tick(60)
pygame.init()