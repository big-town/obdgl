#define LEDPIN 2
volatile byte Comp=0;
int inBt;
void setup() {
  Serial.begin(38400);
  pinMode(LEDPIN,OUTPUT);
  digitalWrite(LEDPIN,LOW);  
  sei(); //глобальное разрешение прерываний
  ACSR|=(1<<ADC);  //включение компаратора
  ACSR|=(0<<ACBG);  //не используем внутренний источник опорного напряжения
  ACSR|=(1<<ACIE); //разрешение прерываний
  ACSR|=(1<<ACIS0)|(1<<ACIS1); //прерывание по нарастающему фронту
}

void loop() {
  if (Serial.available() > 0) {  //если есть доступные данные
    while ((inBt = Serial.read())!=-1); //Serial.print(inBt);
   //if(Comp>64) Comp=0;
   //if (Serial.read()!=-1)
    Serial.write(Comp);
    Comp=0;        
  }
  digitalWrite(LEDPIN,LOW);
}

ISR(ANALOG_COMP_vect)
{
  Comp++;
  digitalWrite(LEDPIN,HIGH);            
}
