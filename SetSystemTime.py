#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
import datetime as dt
import time as tm
from Tkinter import *
import tkFileDialog as filedialog
import tkMessageBox
import subprocess
import threading
#from accelerate6 import PaintCompGraph

def ButtonSetClick():
    MM="%02d"% SetMon.get()
    DD="%02d"% SetDay.get()
    hh="%02d"% SetHour.get()
    mmin="%02d"% SetMin.get()
    YY="%02d"% SetYear.get()
    subprocess.call('sudo date '+str(MM)+str(DD)+str(hh)+str(mmin)+str(YY), shell=True)

def ButtonExitClick():
	global STOP
	STOP=0
	sys.exit()

def print_date():
    while STOP:
	LabelTime.configure(text= tm.ctime())
	tm.sleep(1)

SetTime = Tk()
SetTime.title('Set time')
#SetTime.geometry('480x280+0+0')
SetTime.geometry('%dx%d+%d+%d' % (480, 320, 0, -8))
SetTime.configure(cursor='none')
#attributes('-fullscreen', True)
LabelTime=Label(SetTime,text= tm.ctime(),height=2)
LabelTime.grid(row=0,column=0,columnspan=6)

NowTime=tm.localtime()
print NowTime.tm_year

SetYear = Scale(SetTime, from_=2016, to=2050, label="year",width=30,length=190)
SetYear.set(NowTime.tm_year)
SetYear.grid(row=1,column=0)

SetMon = Scale(SetTime, from_=1, to=12,  label="mon",width=30,length=190)
SetMon.set(NowTime.tm_mon)
SetMon.grid(row=1,column=1)

SetDay = Scale(SetTime, from_=1, to=31, label="day",width=30,length=190)
SetDay.set(NowTime.tm_mday)
SetDay.grid(row=1,column=2)

SetHour = Scale(SetTime, from_=0, to=23, label="hour",width=30,length=190)
SetHour.set(NowTime.tm_hour)
SetHour.grid(row=1,column=3)

SetMin = Scale(SetTime, from_=0, to=59,  label="min",width=30,length=190)
SetMin.set(NowTime.tm_min)
SetMin.grid(row=1,column=4)

SetButton=Button(SetTime,text="Set",command=ButtonSetClick,height=2,width=10)
ExitButton=Button(SetTime,text="Exit",command=ButtonExitClick,height=2,width=10)

SetButton.grid(row=2,column=0)
ExitButton.grid(row=2,column=1)

STOP=1
Thread_print=threading.Thread(target=print_date)
Thread_print.daemon = False
Thread_print.start()

SetTime.mainloop()
