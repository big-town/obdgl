#!/usr/bin/python
import time
import serial
import struct
class chDetonation:
    FlagWrite=False
    tb=0
    isOpen=False

    def __init__(self,port='/dev/ttyUSB1',baudrate=38400):
        try:
            self.ser = serial.Serial(
                port,
                baudrate,
                timeout=0
            )
            self.isOpen=True
        except Exception:
            self.isOpen=False
            print('Channel detonation will ignore.')

        self.tb=time.time()
        
    def __del__(self):
        if self.isOpen:
            self.ser.close()

    def get(self):
        countByte=0 
        if (time.time()-self.tb)>1:
            self.FlagWrite=False
        if self.ser.inWaiting()==0 and self.ser.out_waiting==0 and not self.FlagWrite: 
            self.tb=time.time()
            self.ser.write('7')
            self.ser.flush()
            self.FlagWrite=True
        while countByte==0 and (time.time()-self.tb)<=1:
            countByte=self.ser.inWaiting()
        if countByte>0:
            data = self.ser.read(1) 
            b=struct.unpack('B',data[0])
            #print b[0], "%10.6f" % time.time()
            self.FlagWrite=False
            return int(b[0])
        return -1
 
