#!/usr/bin/python
from __future__ import division
# -*- coding: utf-8 -*-

# from PIL import Image, ImageDraw, ImageTk, ImageFont
import Tkinter as tk
import threading
import time
import sys
import obd
#import chDeton
# global gif,rotated,ImageTk,image,tk_img, my_count, new_img, pen

# my_count=1


STOP_GET = False
"""
GET_RPM=3800
GET_SPEED=120
GET_SPEED_EXACTLY=0
GET_LOAD=100
GET_LOAD_ABS=100
GET_THROTTLE=100
GET_THROTTLE_ABS=100
GET_SHORT_L=1.6
GET_LONG_L=2.3
GET_TIMING=17.5
GET_TEMP=92
GET_FUEL_STATUS="Open loop acceleration"
GET_ACCEL=0.0
"""
GET_RPM = "0"
GET_SPEED = "0"
GET_LOAD = "0"
GET_LOAD_ABS = "0"
GET_THROTTLE = "0"
GET_THROTTLE_ABS = "0"
GET_SHORT_L = "0"
GET_LONG_L = "0"
GET_TIMING = "0"
GET_TEMP = "0"
GET_FUEL_STATUS = " "
GET_FUEL_LEVEL = "0"
GET_AFR = "0"
TIME = "0"
GET_FUEL_STATUS = ""
LEVEL_DETON="0"



def getValues():
    global GET_FUEL_STATUS, GET_AFR, GET_SPEED, GET_FUEL_STATUS, GET_TEMP, GET_RPM, GET_SPEED, GET_LOAD, GET_LOAD_ABS, GET_THROTTLE, GET_THROTTLE_ABS, GET_SHORT_L, GET_LONG_L, GET_TIMING, LEVEL_DETON

    logFilename = time.strftime("%d-%m-%Y_%H%M%S")
    logFile = open(logFilename + "l.csv", "w")
    logFile.write(
        "TIME,GET_SPEED,GET_RPM,GET_TIMING,GET_AFR,GET_FUEL_STATUS ,GET_TEMP,GET_LOAD,GET_LOAD_ABS,GET_THROTTLE,GET_THROTTLE_ABS,GET_SHORT_L,GET_LONG_L,LEVEL_DETON\n"
    )
    while STOP_GET:
        cmd = obd.commands.FUEL_STATUS  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_FUEL_STATUS = str(response.value)

        cmd = obd.commands.RPM  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_RPM = str(response.value)

        cmd = obd.commands.SPEED  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            #GET_SPEED = response.value
            GET_SPEED=str(response.value)

        cmd = obd.commands.TIMING_ADVANCE  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_TIMING = str(response.value)

        cmd = obd.commands.SHORT_FUEL_TRIM_1  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_SHORT_L = str(response.value)

        # select an OBD command (sensor)
        cmd = obd.commands.RELATIVE_THROTTLE_POS
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_THROTTLE = str(response.value)

        cmd = obd.commands.THROTTLE_POS  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_THROTTLE_ABS = str(response.value)

        cmd = obd.commands.LONG_FUEL_TRIM_1  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_LONG_L = str(response.value)

        cmd = obd.commands.ENGINE_LOAD  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_LOAD = str(response.value)

        cmd = obd.commands.ABSOLUTE_LOAD  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_LOAD_ABS = str(response.value)

        cmd = obd.commands.COOLANT_TEMP  # select an OBD command (sensor)
        # send the command, and parse the response
        response = connection.query(cmd)
        if response.value != None:
            GET_TEMP = str(response.value)

        # select an OBD command (sensor)
        cmd = obd.commands.COMMAND_EQUIV_RATIO
        # send the command, and parse the response
        response = connection.query(cmd)
        # print response.value
        if response.value != None:
            GET_AFR = str(14.7/response.value)

        #if chDt.isOpen:
         #   LEVEL_DETON=str(chDt.get())
          #  print type(LEVEL_DETON)
        cmd = obd.commands.KNOCK_TIMING
        response = connection.query(cmd,True)
        if response.value != None:
            LEVEL_DETON = str(response.value)

        TIME = str(time.time())
        RESULT = TIME+','+GET_SPEED+','+GET_RPM+','+GET_TIMING+','+GET_AFR+','+GET_FUEL_STATUS + ','+GET_TEMP + \
            ','+GET_LOAD+','+GET_LOAD_ABS+','+GET_THROTTLE+',' + \
            GET_THROTTLE_ABS+','+GET_SHORT_L+','+GET_LONG_L+','+LEVEL_DETON+"\n"
        #GET_FUEL_STATUS="Open temp"
        loopState=stLoop.get()
        if loopState == 0:
            logFile.write(RESULT)
        if (loopState == 1) and (GET_FUEL_STATUS.find('Close', 0, 7) >= 0):
            logFile.write(RESULT)
        if (loopState == 2) and (GET_FUEL_STATUS.find('Open', 0, 7) >= 0):
            logFile.write(RESULT)
        #print "thread",STOP_GET, loopState,logFile,ThreadgetValues.isAlive()
        #time.sleep(1)

    logFile.close()


def quit(event=None):
    global STOP_GET
    STOP_GET = False
    connection.close()
    # print "Quit"+','+event
    # t.kill()
    sys.exit(0)


def logStop():
    btnStop.config(state=tk.DISABLED)
    btnStart.config(state=tk.NORMAL)
    global STOP_GET
    STOP_GET = False
    #ThreadgetValues.kill()
    #while ThreadgetValues.isAlive():
    #   time.sleep(0.5)
    #print "logStop",ThreadgetValues.isAlive(),STOP_GET
    #time.sleep(15)



def logStart():
    global ThreadgetValues,STOP_GET, logFile

    btnStart.config(state=tk.DISABLED)
    btnStop.config(state=tk.NORMAL)



    STOP_GET = True
    ThreadgetValues = threading.Thread(target=getValues)
    ThreadgetValues.daemon = False
    ThreadgetValues.start()

ThreadgetValues = None
root = tk.Tk()
root.grid_columnconfigure(1, weight=1)
root.grid_columnconfigure(0, weight=1)

btnQuit = tk.Button(root,
                    text="Quit",
                    font=("Courier", 44),
                    width=6,
                    pady=15,
                    fg="red", comman=quit)
btnQuit.grid(row=0, column=0)
btnStart = tk.Button(root,
                     text="Start",
                     font=("Courier", 44),
                     width=6,
                     pady=15,
                     fg="green", comman=logStart)
btnStart.grid(row=1, column=0)
btnStop = tk.Button(root,
                    text="Stop",
                    font=("Courier", 44),
                    width=6,
                    pady=15,
                    fg="green", comman=logStop, state=tk.DISABLED)
btnStop.grid(row=2, column=0)

# stLoop =  0
# stLoop.set(0)
stLoop = tk.IntVar()
stLoop.set(0)

rbAll = tk.Radiobutton(text="all", font=("Courier", 44), width=5,
                       indicatoron=0, variable=stLoop, value=0, background='grey').grid(row=0, column=1)
rbClose = tk.Radiobutton(text="Close", font=(
    "Courier", 44), width=5, indicatoron=0, variable=stLoop, value=1, background='grey').grid(row=1, column=1)
rbOpen = tk.Radiobutton(text="Open", font=("Courier", 44), width=5,
                        indicatoron=0, variable=stLoop, value=2, background='grey', highlightcolor='green').grid(row=2, column=1)


# connection=0

# while !connection:
connection = obd.OBD("/dev/ttyUSB0")  # auto-connects to USB or RF port
#chDt=chDeton.chDetonation("/dev/ttyUSB1")
# time.sleep(3)
# connection.close()
#	cmd = obd.commands.RPM # select an OBD command (sensor)
#	response = connection.query(cmd) # send the command, and parse the response
#	print(response.value)
#	print(response.unit)


# root.attributes('-fullscreen', True)
root.geometry("480x320")  # Set the window dimensions
root.configure(background='black')
# root.bind('<KeyPress>', onKeyPress)

root.mainloop()
